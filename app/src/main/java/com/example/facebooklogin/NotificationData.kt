package com.example.facebooklogin

data class NotificationData(
    val title:String,
    val message:String,
)
