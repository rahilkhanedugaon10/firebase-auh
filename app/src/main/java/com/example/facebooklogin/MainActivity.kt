package com.example.facebooklogin

import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
const val TOPIC = "/topics/myTopic"
class MainActivity : AppCompatActivity() {
    private val TAG ="MainActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        FirebaseMessaging.getInstance().subscribeToTopic(TOPIC)
        val etTitle = findViewById<EditText>(R.id.etTitle)
        val etMessage = findViewById<EditText>(R.id.etMessage)
        val btn_send = findViewById<Button>(R.id.btn_send)
        btn_send.setOnClickListener{
            val title = etTitle.text.toString()
            val message = etMessage.text.toString()
            if (title.isNotEmpty() && message.isNotEmpty()){
                PushNotification(
                    NotificationData(title,message),
                    TOPIC
                ).also {
                    SendNotfication(it)
                }
            }
        }
    }
    private fun SendNotfication(notification: PushNotification)= CoroutineScope(Dispatchers.IO).launch {
        try {
                val response = RetrofitInstance.api.postNotification(notification)
                   if (response.isSuccessful){
                       Log.d(TAG,"Response:${Gson().toJson(response)}")
                   }else{
                       Log.e(TAG,response.errorBody().toString())
                   }
        }catch (e:Exception){
           Log.e(TAG,e.toString())

        }
    }

}